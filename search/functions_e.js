var searchData=
[
  ['sawwave_0',['SawWave',['../_lab1__1_80_8py.html#acc198734fdb23a83be7c937137f15775',1,'Lab1_1']]],
  ['scan3_1',['scan3',['../classplate_1_1_touch_panel.html#ad9ee377612e9ed2227af44c1e78748a5',1,'plate::TouchPanel']]],
  ['set_5fcal_5fcoeff_2',['set_cal_coeff',['../classplate_1_1_touchpad.html#afc7080a668852e74e58ba155cbce834f',1,'plate::Touchpad']]],
  ['set_5fduty_3',['set_duty',['../class_motor_driver_1_1_motor.html#acc7555a78d6ec04e342f20379bf67116',1,'MotorDriver.Motor.set_duty(self, duty)'],['../class_motor_driver_1_1_motor.html#acc7555a78d6ec04e342f20379bf67116',1,'MotorDriver.Motor.set_duty(self, duty)'],['../class_motor_driver5_1_1_motor.html#ae9a396cdf1abd6a92f6e7346eff0e4c7',1,'MotorDriver5.Motor.set_duty()'],['../class_motor_driver_1_1_motor.html#acc7555a78d6ec04e342f20379bf67116',1,'MotorDriver.Motor.set_duty()']]],
  ['set_5fgain_4',['set_Gain',['../classclosedloop_1_1_closed_loop.html#a9977f9a73888261059c38e25671d0cbc',1,'closedloop.ClosedLoop.set_Gain()'],['../classclosedloop5_1_1_closed_loop.html#a67d58fd1733d418a589c77374b51ebce',1,'closedloop5.ClosedLoop.set_Gain()']]],
  ['set_5fkd_5',['set_Kd',['../classclosedloop_1_1_closed_loop.html#aae60c53ecf1c8a238cd0c7912cde23b4',1,'closedloop::ClosedLoop']]],
  ['set_5fki_6',['set_Ki',['../classclosedloop_1_1_closed_loop.html#a65e9afebc1297a781ef7cb1eaba9037a',1,'closedloop::ClosedLoop']]],
  ['set_5fkp_7',['set_Kp',['../classclosedloop_1_1_closed_loop.html#a617a88880b37c7434947936e1d3a37ce',1,'closedloop::ClosedLoop']]],
  ['set_5fmode_8',['set_mode',['../class_i_m_u_1_1_b_n_o055.html#acf8e0e50f232531653c4fb972f38fc8a',1,'IMU::BNO055']]],
  ['set_5freference_9',['set_reference',['../classclosedloop_1_1_closed_loop.html#a2dc26ed336ac1f7c6d46d6a282d39726',1,'closedloop::ClosedLoop']]],
  ['set_5freference_10',['set_Reference',['../classclosedloop_1_1_closed_loop.html#ae280c2736be4764a84235da504431ca3',1,'closedloop.ClosedLoop.set_Reference()'],['../classclosedloop5_1_1_closed_loop.html#a01081abceccf08aaeaceb4a00a3f874b',1,'closedloop5.ClosedLoop.set_Reference()']]],
  ['sinewave_11',['SineWave',['../_lab1__1_80_8py.html#a268d1a76daa811091191a8597f2beb65',1,'Lab1_1']]],
  ['squarewave_12',['SquareWave',['../_lab1__1_80_8py.html#a924790d393248e030c3336ac89c53d59',1,'Lab1_1']]]
];
