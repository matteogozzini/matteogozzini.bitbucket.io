var searchData=
[
  ['get_0',['get',['../classshares_1_1_queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares.Queue.get(self)'],['../classshares_1_1_queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares.Queue.get(self)'],['../classshares5_1_1_queue.html#a23dd15888f5253a3e8dc3a5fc1f0dcc2',1,'shares5.Queue.get()'],['../classshares_1_1_queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares.Queue.get()']]],
  ['get_5factuation_1',['get_Actuation',['../classclosedloop_1_1_closed_loop.html#ad24ed6b0ba7a0b04e45d9265e99cde80',1,'closedloop.ClosedLoop.get_Actuation()'],['../classclosedloop5_1_1_closed_loop.html#afc536844efcc1da7a7fafa57cd1eda79',1,'closedloop5.ClosedLoop.get_Actuation()']]],
  ['get_5fcal_5fcoeff_2',['get_cal_coeff',['../class_i_m_u_1_1_b_n_o055.html#af340572349e139c749408829c938d8d7',1,'IMU::BNO055']]],
  ['get_5fdelta_3',['get_delta',['../class_encoder_1_1_encoder.html#a7bf293682012aeef8fd2e12ae9deb383',1,'Encoder.Encoder.get_delta()'],['../class_encoder___lab3_1_1_encoder.html#a1ce9bb253dedc814582dd1688d0e6c40',1,'Encoder_Lab3.Encoder.get_delta()'],['../classencoder_1_1_encoder.html#a2f451b6cb3e85e03d45e0ac097e29a29',1,'encoder.Encoder.get_delta(self)']]],
  ['get_5fdeltatime_4',['get_deltaTime',['../classencoder_1_1_encoder.html#aa34f4c02c82b48e615bdd5acdac8d37f',1,'encoder::Encoder']]],
  ['get_5fkp_5',['get_Kp',['../classclosedloop_1_1_closed_loop.html#aabfd126eb373a40747f7fd312ed0056c',1,'closedloop.ClosedLoop.get_Kp()'],['../classclosedloop5_1_1_closed_loop.html#a1fefaee78fe69f04ec4b2dd844ce8d06',1,'closedloop5.ClosedLoop.get_Kp()']]],
  ['get_5fpos_6',['get_pos',['../class_encoder_1_1_encoder.html#a6008c4ce2253d0475508e7fef338f9bb',1,'Encoder.Encoder.get_pos()'],['../class_encoder___lab3_1_1_encoder.html#a3eb741e7a09d27c30cb18a1915381188',1,'Encoder_Lab3.Encoder.get_pos()']]],
  ['get_5fposition_7',['get_position',['../classencoder_1_1_encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder::Encoder']]],
  ['get_5freference_8',['get_Reference',['../classclosedloop_1_1_closed_loop.html#a1e8661da077f6829bb7e784a3a52eb71',1,'closedloop.ClosedLoop.get_Reference()'],['../classclosedloop5_1_1_closed_loop.html#aa3762edb369caa4159237b040e942d52',1,'closedloop5.ClosedLoop.get_Reference()']]],
  ['get_5fvel_9',['get_vel',['../classencoder_1_1_encoder.html#a2ef9460483583e201ebb6732c4ba2fa5',1,'encoder::Encoder']]],
  ['gyro_5fread_10',['gyro_read',['../class_i_m_u_1_1_b_n_o055.html#a55af5ab9e5701a39d5b441c39373d718',1,'IMU::BNO055']]]
];
