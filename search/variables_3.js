var searchData=
[
  ['delta_0',['delta',['../class_encoder_1_1_encoder.html#a07b54c74b92b26ecefbb7576972ca1f6',1,'Encoder.Encoder.delta()'],['../class_encoder___lab3_1_1_encoder.html#a4f2f3b37a42bc86f0a74db09d9f81dd7',1,'Encoder_Lab3.Encoder.delta()']]],
  ['delta_1',['DELTA',['../user__task_lab3_8py.html#adb9d54ccb43fdcea4bcba3f3ed4bd5d0',1,'user_taskLab3']]],
  ['derivative_2',['derivative',['../classclosedloop_1_1_closed_loop.html#aa508e71d17db2f1e3da24fc26d127ee4',1,'closedloop::ClosedLoop']]],
  ['displayp_3',['displayp',['../classuser__task_lab3_1_1_task_user.html#a3d9cbeb90cc11a77022756a6ad520476',1,'user_taskLab3::TaskUser']]],
  ['displayposition_4',['displayPosition',['../classtask__user_1_1_task___user.html#af821b8ce78463df34ba3b6291409f05a',1,'task_user::Task_User']]],
  ['displaypositiontime_5',['DisplayPositionTime',['../classtask__user_1_1_task___user.html#a23f9d2b5e4e5c83179e14ca6e1373a90',1,'task_user.Task_User.DisplayPositionTime()'],['../classuser__task_lab3_1_1_task_user.html#a99daea1f7abd106f4429c78e2d29f56d',1,'user_taskLab3.TaskUser.DisplayPositionTime()']]],
  ['duty_6',['DUTY',['../_lab_013_2_motor__task_8py.html#a5288ee2e53224ef9bc4bbe38ffe692c3',1,'motor_task.DUTY()'],['../user__task_lab3_8py.html#a6ab6bcb0087dc406d6a10500917002cf',1,'user_taskLab3.DUTY()']]],
  ['duty1_7',['duty1',['../_lab_014_2main_8py.html#a4f911a1828df7baf0a3e060305223057',1,'main']]],
  ['duty2_8',['duty2',['../_lab_014_2main_8py.html#ac52b4d68543efdb75c86ea26b8171085',1,'main']]]
];
